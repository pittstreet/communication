package com.example.zx43.pittstreetemerge.Activity;

import android.content.Context;
import android.os.PowerManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.net.wifi.WpsInfo;
import android.net.wifi.p2p.WifiP2pConfig;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pDeviceList;
import android.net.wifi.p2p.WifiP2pInfo;
import android.net.wifi.p2p.WifiP2pManager;
import android.os.AsyncTask;
import android.os.Looper;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.net.wifi.p2p.WifiP2pManager.ActionListener;
import com.example.zx43.pittstreetemerge.Adapter.MyAdapter;
import com.example.zx43.pittstreetemerge.BroadcastReceiver.WifiDirectBroadcastReceiver;
import com.example.zx43.pittstreetemerge.R;
import com.example.zx43.pittstreetemerge.Service.DataTransferService;
import com.example.zx43.pittstreetemerge.Service.FileTransferService;
import com.example.zx43.pittstreetemerge.StepDetection.MainProcess;
import com.example.zx43.pittstreetemerge.Task.DataServerAsyncTask;
import com.example.zx43.pittstreetemerge.Task.FileServerAsyncTask;
import com.example.zx43.pittstreetemerge.Utils.Utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;

public class MainActivity extends AppCompatActivity {

    private Button discover;
    private Button stopdiscover;
    private Button stopconnect;
    private Button begrouppwener;

    private Switch switchStepDetection;
    private TextView txtNumber;

    private PowerManager powerManager;
    private PowerManager.WakeLock wakeLock;

    private boolean detectMotion;


    private RecyclerView mRecyclerView;
    private MyAdapter mAdapter;
    private List peers = new ArrayList();
    private List<HashMap<String, String>> peersshow = new ArrayList();
    public int mycount=0;
    private WifiP2pManager mManager;
    private WifiP2pManager.Channel mChannel;
    private BroadcastReceiver mReceiver;
    private IntentFilter mFilter;
    private WifiP2pInfo info;
    private static final String TAG = WifiP2pManager.ActionListener.class.getSimpleName();
    private FileServerAsyncTask mServerTask;
    private DataServerAsyncTask mDataTask;
    private boolean isSupported = true;
    private static boolean isEnabled;
    private Utils utils;
    boolean isWifiEnabled;
    public WifiP2pManager.ConnectionInfoListener mInfoListener;
    public WifiP2pManager.PeerListListener mPeerListListerner;
    public DataTransferService Test = new DataTransferService();
    private boolean _register = false;
    public Intent serviceIntent;
    public boolean EnSetUp = false;



    public static void setIsWifiP2pEnabled(boolean bool) {
        isEnabled = bool;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
        initIntentFilter();
        initReceiver();
        initEvents();
        Log.i("xyz", "onCreate Finished");
    }
    class TaggedActionListener implements ActionListener {

        private final String tag;

        public TaggedActionListener(String tag) {
            this.tag = tag;
        }

        @Override
        public void onFailure(int reason) {
            Log.e(TAG, "Error during Wifi P2P operation. operation " + tag + " error " +  reason);
            if (reason == WifiP2pManager.P2P_UNSUPPORTED) {
                Log.e(TAG, "Wifi P2P not supported");
                isSupported = false;
                isEnabled = false;
            }

        }

        @Override
        public void onSuccess() {
            Log.d(TAG, "Wifi P2P operation " + tag + " success");
        }

    }
    private void initEvents() {

        discover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DiscoverPeers();
            }
        });
        begrouppwener.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BeGroupOwener();
            }
        });

        stopdiscover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StopDiscoverPeers();
            }
        });
        stopconnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StopConnect();
            }
        });


        mAdapter.SetOnItemClickListener(new MyAdapter.OnItemClickListener() {
            @Override
            public void OnItemClick(View view, int position) {
                CreateConnect(peersshow.get(position).get("address"),
                        peersshow.get(position).get("name"));
            }

            @Override
            public void OnItemLongClick(View view, int position) {
            }
        });
    }

    public void SenData()
    {
        serviceIntent = new Intent(MainActivity.this,
                DataTransferService.class);

        serviceIntent.setAction(DataTransferService.ACTION_SEND_FILE);

        serviceIntent.putExtra(DataTransferService.EXTRAS_GROUP_OWNER_ADDRESS,
                info.groupOwnerAddress.getHostAddress());
        Log.i("address", "owenerip is " + info.groupOwnerAddress.getHostAddress());
        serviceIntent.putExtra(DataTransferService.EXTRAS_GROUP_OWNER_PORT,
                8888);
        Test.getData(mycount);
        MainActivity.this.startService(serviceIntent);
    }

    private void BeGroupOwener() {
        mManager.createGroup(mChannel, new WifiP2pManager.ActionListener() {
            @Override
            public void onSuccess() {

            }

            @Override
            public void onFailure(int reason) {

            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 20) {
            super.onActivityResult(requestCode, resultCode, data);
            Uri uri = data.getData();
            Intent serviceIntent = new Intent(MainActivity.this,
                    FileTransferService.class);

            serviceIntent.setAction(FileTransferService.ACTION_SEND_FILE);
            serviceIntent.putExtra(FileTransferService.EXTRAS_FILE_PATH,
                    uri.toString());

            serviceIntent.putExtra(FileTransferService.EXTRAS_GROUP_OWNER_ADDRESS,
                    info.groupOwnerAddress.getHostAddress());
            serviceIntent.putExtra(FileTransferService.EXTRAS_GROUP_OWNER_PORT,
                    8988);
            MainActivity.this.startService(serviceIntent);
        }
    }

    private void StopConnect() {
        SetButtonGone();
        mManager.removeGroup(mChannel, new TaggedActionListener("removeGroup"));
        mManager = (WifiP2pManager) getSystemService(WIFI_P2P_SERVICE);
        mChannel = mManager.initialize(this, Looper.myLooper(), null);
        if(_register) {
            unregisterReceiver(mReceiver);
            _register=false;
        }
        mReceiver = new WifiDirectBroadcastReceiver(mManager, mChannel, this, mPeerListListerner, mInfoListener);
        if(!_register) {
            registerReceiver(mReceiver, mFilter);
            _register = true;
        }
        mAdapter.RefreshView();
        txtNumber.setText("Detect Step");
        switchStepDetection.setChecked(false);
        StopDiscoverPeers();
    }

    /*A demo base on API which you can connect android device by wifidirect,
    and you can send file or data by socket,what is the most important is that you can set
    which device is the client or service.*/

    private void CreateConnect(String address, final String name) {
        WifiP2pDevice device;
        WifiP2pConfig config = new WifiP2pConfig();
        Log.i("xyz", address);

        config.deviceAddress = address;
        /*mac地址*/

        config.wps.setup = WpsInfo.PBC;
        Log.i("address", "MAC IS " + address);
        if (address.equals("9a:ff:d0:23:85:97")) {
            config.groupOwnerIntent = 0;
            Log.i("address", "lingyige shisun");
        }
        if (address.equals("36:80:b3:e8:69:a6")) {
            config.groupOwnerIntent = 15;
            Log.i("address", "lingyigeshiwo");

        }

        Log.i("address", "lingyige youxianji" + String.valueOf(config.groupOwnerIntent));

        mManager.connect(mChannel, config, new WifiP2pManager.ActionListener() {

            @Override
            public void onSuccess() {

            }

            @Override
            public void onFailure(int reason) {


            }
        });
    }

    private void StopDiscoverPeers() {
        mManager.stopPeerDiscovery(mChannel, new WifiP2pManager.ActionListener() {
            @Override
            public void onSuccess() {
            }

            @Override
            public void onFailure(int reason) {


            }
        });
    }

    private void initView() {
        begrouppwener= (Button) findViewById(R.id.bt_bgowner);
        stopdiscover = (Button) findViewById(R.id.bt_stopdiscover);
        discover = (Button) findViewById(R.id.bt_discover);
        stopconnect = (Button) findViewById(R.id.bt_stopconnect);
        powerManager = (PowerManager)getSystemService(Context.POWER_SERVICE);
        wakeLock = powerManager.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK | PowerManager.ON_AFTER_RELEASE, "DataCollecting");
        txtNumber = (TextView) findViewById(R.id.txtNumber);
        final MainActivity thisActivity = this;
        switchStepDetection = (Switch) findViewById(R.id.switchStepDetection);
        switchStepDetection.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    wakeLock.acquire();
                    detectMotion = true;
                    new MainProcess(thisActivity).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                } else {
                    detectMotion = false;
                    wakeLock.release();
                }
            }
        });
        switchStepDetection.setVisibility(View.GONE);
        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerview);
        mAdapter = new MyAdapter(peersshow);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setLayoutManager(new LinearLayoutManager
                (this.getApplicationContext()));

    }

    private void initReceiver() {
        Log.i("initReceiver","The initReceiver is called");
        mManager = (WifiP2pManager) getSystemService(WIFI_P2P_SERVICE);
        mChannel = mManager.initialize(this, Looper.myLooper(), null);

        mPeerListListerner = new WifiP2pManager.PeerListListener() {
            @Override
            public void onPeersAvailable(WifiP2pDeviceList peersList) {
                peers.clear();
                peersshow.clear();
                Collection<WifiP2pDevice> aList = peersList.getDeviceList();
                peers.addAll(aList);

                for (int i = 0; i < aList.size(); i++) {
                    WifiP2pDevice a = (WifiP2pDevice) peers.get(i);
                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put("name", a.deviceName);
                    map.put("address", a.deviceAddress);
                    peersshow.add(map);
                }
                mAdapter = new MyAdapter(peersshow);
                mRecyclerView.setAdapter(mAdapter);
                mRecyclerView.setLayoutManager(new LinearLayoutManager
                        (MainActivity.this));
                mAdapter.SetOnItemClickListener(new MyAdapter.OnItemClickListener() {
                    @Override
                    public void OnItemClick(View view, int position) {
                        CreateConnect(peersshow.get(position).get("address"),
                                peersshow.get(position).get("name"));

                    }

                    @Override
                    public void OnItemLongClick(View view, int position) {

                    }
                });
            }
        };

         mInfoListener = new WifiP2pManager.ConnectionInfoListener() {

            @Override
            public void onConnectionInfoAvailable(final WifiP2pInfo minfo) {

                Log.i("xyz", "InfoAvailable is on");
                info = minfo;
                TextView view = (TextView) findViewById(R.id.txtNumber);
                if (info.groupFormed && info.isGroupOwner) {
                    Log.i("xyz", "owner start");

                    //mServerTask = new FileServerAsyncTask(MainActivity.this, view);
                    //mServerTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

                    mDataTask = new DataServerAsyncTask(MainActivity.this, view);
                    mDataTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    Log.i("Data Task Async","The new DataServerAsyncTask is built in initReceiver()");
                } else if (info.groupFormed) {
                    SetButtonVisible();
                }
            }
        };

        mReceiver = new WifiDirectBroadcastReceiver(mManager, mChannel, this, mPeerListListerner, mInfoListener);
        _register =true;
    }

    private void SetButtonVisible() {
        switchStepDetection.setVisibility(View.VISIBLE);

    }

    private void SetButtonGone() {
        switchStepDetection.setVisibility(View.GONE);
    }


    private void DiscoverPeers() {
        mManager.discoverPeers(mChannel, new WifiP2pManager.ActionListener() {
            @Override
            public void onSuccess() {
            }

            @Override
            public void onFailure(int reason) {
            }
        });
    }

    private void initIntentFilter() {
        mFilter = new IntentFilter();
        mFilter.addAction(WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION);
        mFilter.addAction(WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION);
        mFilter.addAction(WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION);
        mFilter.addAction(WifiP2pManager.WIFI_P2P_DISCOVERY_CHANGED_ACTION);
        mFilter.addAction(WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i("xyz", "onResume");
        registerReceiver(mReceiver, mFilter);
        _register = true;
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.i("xyz", "OnPause");
        unregisterReceiver(mReceiver);
        _register = false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i("xyz", "onDestroy");
        StopConnect();
    }

    public void ResetReceiver() {
        unregisterReceiver(mReceiver);
        registerReceiver(mReceiver, mFilter);
        _register = true;
    }

    public boolean isDetectMotion() {
        return detectMotion;
    }

    public TextView getTxtNumber() {
        return txtNumber;
    }
    private static void setDefaultUncaughtExceptionHandler() {
        try {
            Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {

                @Override
                public void uncaughtException(Thread t, Throwable e) {
                    Log.e("detected in thread ", t.toString(), e);
                }
            });
        } catch (SecurityException e) {
            Log.e("noDefaultExHandler", e.toString());
        }
    }





}
