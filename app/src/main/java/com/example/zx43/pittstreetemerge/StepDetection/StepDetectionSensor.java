package com.example.zx43.pittstreetemerge.StepDetection;
/**
 * Name: Pitt Street
 * Date created: 7/05/16
 * Version: 1.0
 */


import android.app.Activity;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;

//This class detects foot steps of the user when a phone is strapped to their leg near the foot
public class StepDetectionSensor extends Activity implements SensorEventListener {
    private final float GRAVITY = 9.807F;
    private final int SENSITIVITY = 4; //In m/s^2
    private final int TIMECONST = 500; //In Milliseconds

    private SensorManager sensorManager;
    private Sensor accelerometer;

    private long currentTime;
    private long timePassed;
    private boolean isWaiting;
    private boolean isStepDetected;

    public StepDetectionSensor(Activity mainActivity) {
        sensorManager = (SensorManager)mainActivity.getSystemService(SENSOR_SERVICE);
        accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
    }

    //Return whether or not a step was detected
    public boolean isStepDetected() {
        boolean result = isStepDetected;
        isStepDetected = false;
        return result;
    }

    public void startListening() {
        sensorManager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_FASTEST);
        timePassed = 0;
        isStepDetected = false;
        isWaiting = false;
    }

    public void stopListening() {
        sensorManager.unregisterListener(this);
    }

    protected void onResume() {
        super.onResume();
    }

    protected void onPause() {
        super.onPause();
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
    }

    @Override
    //Determine whether or not a step was detected (allowing for on the spot stepping)
    public void onSensorChanged(SensorEvent event) {
        if(event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            float acceleration = event.values[1] - GRAVITY;

            //Detect when it goes below the limit (SENSITIVITY)
            if(acceleration <= -SENSITIVITY && !isWaiting) {
                isWaiting = true;
                currentTime = System.currentTimeMillis();
            } else if(isWaiting) {
                //If waiting for it to go above the limit check if it is above the limit
                //Only if its been less than the time limit
                long lastTime = currentTime;
                currentTime = System.currentTimeMillis();
                timePassed += currentTime - lastTime;

                //If time ran out
                if(timePassed >= TIMECONST) {
                    timePassed = 0;
                    isWaiting = false;
                } else if(acceleration >= SENSITIVITY) {
                    //If happened before time ran out
                    timePassed = 0;
                    isWaiting = false;
                    isStepDetected = true;
                }
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }
}

