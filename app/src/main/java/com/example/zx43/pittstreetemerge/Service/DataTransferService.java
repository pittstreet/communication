// Copyright 2011 Google Inc. All Rights Reserved.

package com.example.zx43.pittstreetemerge.Service;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.p2p.WifiP2pManager;
import android.util.Log;

import com.example.zx43.pittstreetemerge.Activity.MainActivity;

import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.io.InputStream;
/**
 * A service that process each file transfer request i.e Intent by opening a
 * socket connection with the WiFi Direct Group Owner and writing the file
 */
public class DataTransferService extends IntentService {

    private static final int SOCKET_TIMEOUT = 5000;
    public static final String ACTION_SEND_FILE = "com.example.android.wifidirect.SEND_DATA";
    public static final String EXTRAS_GROUP_OWNER_ADDRESS = "sd_go_host";
    public static final String EXTRAS_GROUP_OWNER_PORT = "sd_go_port";
    public static int _count =0;
    private static final String TAG = WifiP2pManager.ActionListener.class.getSimpleName();
    public Socket socket = new Socket();
    public DataTransferService(String name) {
        super(name);
    }

    public DataTransferService() {
        super("DataTransferService");
    }

    /*
     * (non-Javadoc)
     *
     * @see android.app.IntentService#onHandleIntent(android.content.Intent)
     */
    @Override
    protected void onHandleIntent(Intent intent) {

        Context context = getApplicationContext();
        if (intent.getAction().equals(ACTION_SEND_FILE)) {
            if(!socket.isConnected())
            {
                String host = intent.getExtras().getString(
                        EXTRAS_GROUP_OWNER_ADDRESS);

                //Socket socket = new Socket();
                int port = intent.getExtras().getInt(EXTRAS_GROUP_OWNER_PORT);
                try {
                    Log.d(TAG, "Opening client socket - ");
                    socket.bind(null);
                    InetSocketAddress MyAddress = new InetSocketAddress(host, port);
                    socket.connect(MyAddress, SOCKET_TIMEOUT);
                }
                catch(IOException e) {
                    Log.e(TAG, e.getMessage());
                }
            }
                try{
                    Log.d(TAG,
                            "Client socket - " + socket.isConnected());
				     /*returns an output stream to write data into this socket*/
                    OutputStream stream = socket.getOutputStream();
                    String count = String.valueOf(_count);
                    stream.write(count.getBytes());                  ///Changed Afterwards
                    stream.close();
                    Log.i("OutputStream","Clinet side Output stream is closed");
            } catch (IOException e) {
                Log.e(TAG, e.getMessage());
            } finally {
                if (socket != null) {

                    if (socket.isConnected()) {

                       try {
                           //Gracefully Shutdown, maybe not useful for this app
                            /*
                            InputStream is = socket.getInputStream();
                            socket.shutdownOutput(); // Sends the 'FIN' on the network
                            while (is.read() >= 0) ; // "read()" returns '-1' when the 'FIN' is reached
                            is.close();
                            */
                            socket.close();
                            Log.i("Socket","ClientSocketClosed");
                        }
                        catch (IOException e) {
                            // Give up
                            e.printStackTrace();
                        }

                    }
                }
            }

        }
    }


    public void getData(int Count){
        _count = Count;
    }
}
